# download_drawings

This saves about 3 days worth of work by finding all RFI drawings and sketches and placing them into their corresponding folders indicating project roles. 

It determines wether the attachments are drawings by comparing the PDF size to the industry standard drawing size.

Files are renamed according to RFI number, initials of creator, project name, and attachment type (Drawing or Sketch)

